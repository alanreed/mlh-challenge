<?php

namespace App\Http\Middleware;

use Closure;

class SetReferral
{
    /**
     * If the ref number is passed then store it in the user's session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('ref'))
        {
            $request->session()->put('referrer_id', $request->input('ref'));
        }

        return $next($request);
    }
}
