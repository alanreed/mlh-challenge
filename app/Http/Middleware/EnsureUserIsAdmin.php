<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;

class EnsureUserIsAdmin extends \Illuminate\Auth\Middleware\Authenticate
{
    /**
     * Determine if the user is logged in to any of the given guards.
     * Then ensure the user is administrator.
     *
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate(array $guards)
    {
        if (empty($guards)) {
            if ($this->auth->authenticate()->admin) {
                return $this->auth->authenticate();
            }
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards);
    }
}
