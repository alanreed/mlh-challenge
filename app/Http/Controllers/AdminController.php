<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        $users = User::orderBy('id','DESC')->paginate(10);

        $referral_counts = User::select('id', \DB::raw('count(*) as total'))
                            ->whereIn('id', $users->pluck('id'))
                            ->groupBy('referrer_id')
                            ->orderBy('created_at','desc')
                            ->get();

        $total_referrals = User::whereNotNull('referrer_id')->count();

        return view('pages.admin', compact('users','referral_counts','total_referrals'));
    }
}
