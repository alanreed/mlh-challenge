<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function account()
    {
        $user = Auth::user();

        $referred = User::where('referrer_id', $user->id)->orderBy('id','DESC')->paginate(10);

        return view('pages.account', compact('referred'));
    }
}
