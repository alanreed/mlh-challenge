<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserFlowTest extends TestCase
{
	use DatabaseTransactions;

	public function testRedirectToLoginPageIfLoggedOutUserVisitsAccountPage()
	{
	    $response = $this->get('/account')->assertRedirect('/login');
	}

    public function testUserCanLogin()
    {
    	$response = $this->post('/login', ['email' => 'user@example.com', 'password' => 'secret']);

    	$response->assertRedirect('/account');

    	$this->followRedirects($response)->assertSeeText('Welcome tester!');
    }

    public function testLoginWithWrongCredentials()
    {
    	$response = $this->post('/login', 
    		['email' => 'nouser@gmail.com', 'password' => 'NotTheRightPassword'], 
    		['Referer' => '/login']
    	);

    	$response->assertRedirect('/login');

    	$this->followRedirects($response)->assertSeeText('These credentials do not match our records');
    }

    public function testAUserCanRegisterAndViewAccount()
    {
    	$response = $this->post('/register', 
    		[
    			'name' => 'New Test User',
    			'email' => 'testEmail1@example.com', 
    			'password' => 'secret', 
    			'password_confirmation' => 'secret',
    			'referrer_id' => 1 // admin's user id
    		]
    	);

    	$response->assertRedirect('/account');

    	$this->followRedirects($response)->assertSeeText('Welcome New Test User!');
    }

    public function testReferredUserAppearsInReferrersAccount()
    {
    	$response = $this->post('/register', 
    		[
    			'name' => 'New Test User',
    			'email' => 'testEmail1@example.com', 
    			'password' => 'secret', 
    			'password_confirmation' => 'secret',
    			'referrer_id' => 1 // admin's user id
    		]
    	);

    	$response = $this->post('/login', ['email' => 'admin@example.com', 'password' => 'secret']);

    	$response->assertRedirect('/account');

    	// Ensure the name of the referred user shows on the admin's account page.
    	$this->followRedirects($response)->assertSeeText('New Test User');
    }

    /** Follow redirects helper **/
    protected function followRedirects($response)
	{
	    while ($response->isRedirect()) {
	        $response = $this->get($response->headers->get('Location'));
	    }

	    return $response;
	}
}
