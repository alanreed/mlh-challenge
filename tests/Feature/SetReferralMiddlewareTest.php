<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Http\Middleware\SetReferral;
use Illuminate\Http\Request;

class SetReferralMiddlewareTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testReferralTokenWasStoredToSession()
    {
    	$response = $this->get('/?ref=9999');
    	$response->assertSessionHas('referrer_id', 9999);

    	$response = $this->get('register/?ref=1010');
    	$response->assertSessionHas('referrer_id', 1010);
    	
    	$response = $this->get('register');
    	$response->assertSessionHas('referrer_id', 1010);
    }
}
