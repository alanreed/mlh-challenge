## About

This is the solution to MLH Coding Challenge by Alan Reed.

Reference: https://docs.google.com/document/d/1kOF2TZ_b2hA_wdHz4DslJqW3EwbXUwuPAiLYy0j3BHI/edit#

## Tool Used

- Laravel 5.4
- Bootstrap 3 + Bootswatch "flatly" theme

## How set up application

1. Clone the repo.

        git clone git@bitbucket.org:alanreed/mlh-challenge.git

2. Enter the new directory.

        cd mlh-challenge

3. Install dependencies.

        composer install

4. Setup `.env` file and config database connection.

        cp .env.example .env
        php artisan key:gen
        vim .env

5. Run migrations.

        php artisan migrate

6. Seed the database with dummy test data (Optional).

        php artisan db:seed

7. Serve up the application

        php artisan serve

8. Visit `localhost:8000' in your browser.