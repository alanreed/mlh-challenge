<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

    	// Create Admin user
        DB::table('users')->insert([
            'name' => 'Admin Alan',
            'email' => 'admin@example.com',
            'admin' => true,
            'password' => bcrypt('secret'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'name' => 'tester',
            'email' => 'user@example.com',
            'password' => bcrypt('secret'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    	for ($i=0;$i<100;$i++) {
	        DB::table('users')->insert([
	            'name' => $faker->name,
	            'email' => $faker->safeEmail,
	            'password' => bcrypt('secret'),
	            'referrer_id' => rand(2,11),
	            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	        ]);
    	}

    }
}
