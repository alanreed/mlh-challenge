{{------------------------------------------}}
{{--              PAGE CONFIG             --}}
{{------------------------------------------}}

@extends('layouts.default')

@section('seo-meta')
<title>404 Page Not Found</title>
<meta name=Description content=""/>
@stop

{{------------------------------------------}}
{{--                CONTENT               --}}
{{------------------------------------------}}

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>404 Page not found</h1>
    </div>
    <div class="col-md-6">
      <p class="lead"> 
        Don't know what to tell you...
      </p>
    </div>
  </div>
  <div class="row text-center">
	<div class="col-lg-12">
	  <h3>The page you are looking for was not found.</h3>
	  <p>Click here to return to the <a href="/">homepage</a>.</p>
	</div>
  </div>

@stop