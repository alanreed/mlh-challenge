
<div class="container">
  <hr>
  <footer>
    <div class="row">
      <div class="col-lg-12">
        <ul class="list-unstyled">
          <li class="pull-right"><a href="#top">Back to top</a></li>
          <li>MHL Coding Challenge by Alan Reed</li>
        </ul>
      </div>
    </div>
  </footer>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js" integrity="sha256-rsPUGdUPBXgalvIj4YKJrrUlmLXbOb6Cp7cdxn1qeUc=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/js/app.js"></script>
