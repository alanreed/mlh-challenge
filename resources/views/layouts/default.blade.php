<!doctype html>
<html lang="{{ config('app.locale') }}">
	@include('includes.meta')
<body>
    @include('includes.header')
    
    @yield('content')

    @include('includes.footer')
</body>
</html>