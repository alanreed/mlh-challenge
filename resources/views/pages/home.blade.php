{{------------------------------------------}}
{{--              PAGE CONFIG             --}}
{{------------------------------------------}}

@extends('layouts.default')

@section('seo-meta')
<title>MHL Referral Network</title>
<meta name=Description content=""/>
@stop

{{------------------------------------------}}
{{--                CONTENT               --}}
{{------------------------------------------}}

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>MHL Hackathon</h1>
    </div>
    <div class="col-md-6">
      <p class="lead"> 
        Sign up for the hackathon now! You'll have a blast. 
      </p>
    </div>
    <div class="col-md-6 text-center">
      @if (Auth::guest())
        <a href="/register" class="btn btn-success">Sign up now <span class="glyphicon glyphicon-chevron-right"></span></a>
        <a href="/login" class="btn btn-primary">Log back in</a>
      @else
        <a href="/account" class="btn btn-primary left-spaced">View Your Account</a>
      @endif
    </div>
    <hr>
  </div>

  <div class="row text-center">
    <div class="col-md-4">
      <h3><span class="glyphicon glyphicon-heart"></span> Build Something Awesome</h3>
      <p>Spend a weekend hacking something awesome together with your friends. You'll have a blast.</p>
    </div>
    <div class="col-md-4">
      <h3><span class="glyphicon glyphicon-education"></span> Learn All the Things</h3>
      <p>The best way to learn is by doing. Lots of mentors from academia and industry will be on hand to help you!</p>
    </div>
    <div class="col-md-4">
      <h3><span class="glyphicon glyphicon-user"></span> Sign up and Refer Friends</h3>
      <p>Sign up now to secure your spot. Once you sign up you can earn points by referring your friends. The more the merrier!</p>
    </div>
  </div>

  <hr>

  <h3> Demo Login Credentials </h3>
  <ul>
    <li><b>Admin credentials:</b> admin@example.com / secret</li>
    <li><b>User credentials:</b> user@example.com / secret</li>
  </ul>

</div>

@stop
