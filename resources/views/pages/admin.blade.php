@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome <b>{{ Auth::user()->name }}</b>!</div>

                <div class="panel-body">
                    <p>
                        <b>Welcome to the referral tracker admin panel!</b>
                    </p>

                    <hr>

                    <h3>Total Users: {{ $users->total() }} </h3>
                    <h3>Referred Users: {{ $total_referrals }} </h3>

                    <table class="table table-bordered table-striped">
                        <tr><th>Name</th><th>Email</th><th>Was Referred?<th>Referral Count</th><th>Signup Date</th></tr>
                        @foreach ($users as $u)
                            <tr>
                                <td><abbr title="User ID: {{ $u->id }}">{{ $u->name }}</abbr></td>
                                <td><i>{{ $u->email }}</i></td>
                                <td>
                                @if (!is_null($u->referrer_id))
                                    <span class="text-success"><abbr title="User was referred by user ID: {{ $u->referrer_id }}"><span class="glyphicon glyphicon-ok"></span></abbr></span>
                                @else
                                    <span class="text-danger"><abbr title="User was referred by anyone."><span class="glyphicon glyphicon-remove"></span></abbr></span>
                                @endif
                                </td>
                                <td>{{ ( !$referral_counts->where('id', $u->id)->isEmpty() ? ($referral_counts->where('id',$u->id)->first()->total) : 0 ) }}</td>
                                <td>{{ $u->created_at->toFormattedDateString() }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <div class="text-center">
                        {{ $users->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
