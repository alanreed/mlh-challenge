@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome <b>{{ Auth::user()->name }}</b>!</div>

                <div class="panel-body">
                    <p>
                        <b>Welcome to the referral tracker dashboard!</b> You are all signed up for the hackathon event. 
                        In the meantime, you can earn <i>points</i> by referring your friends, and I know how much you like points!
                    </p>
                    <p>
                        You get 10 points for every person who signs up using your referral link. How fun is that?
                    </p>

                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Your referral link:</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" onclick="$(this).select();" value="{{ URL::to('/') }}?ref={{ Auth::user()->id }}">
                        </div>
                      </div>
                    </div>

                    <hr>

                    <h3>You have Earned: {{ $referred->total() * 10 }} points by referring {{ $referred->total() }} people </h3>

                    @if ($referred->total() > 15)
                        <p>Way to go!</p>
                    @elseif ($referred->total() > 5)
                        <p>Not bad, but you can do better.</p>
                    @else
                        <p>You better get to work!</p>
                    @endif


                    <table class="table table-bordered table-striped">
                        <tr><th>Person Referred</th><th>Date Referred</th><th>Points Earned</th></tr>
                        @foreach ($referred as $r)
                            <tr><td>{{ $r->name }}</td><td>{{ $r->created_at->toFormattedDateString() }}</td><td>10</td></tr>
                        @endforeach
                    </table>

                    @if ($referred->isEmpty())
                        <p><b>No referrals yet!</b></p>
                    @endif

                    <div class="text-center">
                        {{ $referred->links() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
